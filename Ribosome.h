#pragma once
#ifndef RIBOSOME_H
#define RIBOSOME_H
#include "Nucleus.h"
#include "Protein.h"
#define MIN_NUM 3

class Ribosome
{
public:
	// methods
	Protein* create_protein(std::string &RNA_transcript) const;
};

#endif /* RIBOSOME_H */