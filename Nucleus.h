#ifndef NUCLEUS_H
#define NUCLEUS_H
#include <string>   // for std::string
#include <iostream>
/*#define FOR_G "C"
#define FOR_C "G"
#define FOR_T "A"
#define FOR_A "T"*/

enum RNA{FOR_G = 'C', FOR_C = 'G', FOR_T = 'A', FOR_A = 'T', FOR_RNA = 'U' };

class Gene
{
public:
	// getters
	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_complementary_dna_strand();
	// setters
	void setStart(const unsigned int start);
	void setEnd(const unsigned int end);
	void set_is_on_complementary_dna_strand(const bool on_complementary_dna_strand);
	// methods
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
private:
	//fields
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;
};

class Nucleus
{
public:
	// getters
	std::string getDNAStrand() const;
	std::string getComplementaryStrand() const;
	std::string get_reversed_DNA_strand() const;
	std::string get_RNA_transcript(const Gene& gene) const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
	// setters
	void setFirstStrand(const std::string newFirstName);
	void setComplementaryStrand(const std::string newSecondName);
	// methods
	void init(const std::string dna_sequence);



private:
	// fields
	std::string _DNA_strand;
	std::string _complementary_DNA_strand;
};
#endif /* NUCLEUS_H */