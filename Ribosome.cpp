#pragma once
#include "Ribosome.h"
/*
The function creates a list of proteins using RNA transcript
Input: RNA transcript
Output: list of proteins
*/
Protein* Ribosome::create_protein(std::string &RNA_transcript) const
{
	int i = 0;
	int j = 0;
	AminoAcid amino = UNKNOWN;
	std::string myRNA = RNA_transcript; //a string to use and not destroy the real RNA transcript
	std::string codon = ""; //a string to use to have three nocleotids
	Protein _protein;
	_protein.init(); //initlize the protein

	
	for (i = 0; i < myRNA.length(); i++)
	{
		if (MIN_NUM <= myRNA.length())
		{ //check if the RNA string is at least 3 characters
			codon = myRNA.substr(0, MIN_NUM); //get the 3 first characters
			myRNA.erase(0, MIN_NUM); //delete the 3 first characters
			amino = get_amino_acid(codon);

			if (UNKNOWN != amino)
			{ //check if get amino acid returns UNKNOWN
				_protein.add(amino);
			}

			else
			{
				std::cout << ":(" << std::endl;
				return NULL;
			}
		}
	}
	return &_protein;
}
