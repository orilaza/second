#ifndef CELL_H
#define CELL_H
#include "Protein.h"
#include "Nucleus.h"
#include "Ribosome.h"
#include "Mitochondrion.h"
#define MIN_AMOUNT 50
#define ATP_UNITS 100

class Cell
{
public:
	//methods
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);
	bool get_ATP();

private:
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glocus_receptor_gene;
	unsigned int _atp_units;
};

#endif