#include "nucleus.h"

/*
Get the RNA and give the complementary strand the RNA of the DNA
Input: the DNA sequence
Output: None
*/
void Nucleus::init(const std::string dna_sequence)
{
	int i = 0;

	for (i = 0; i < dna_sequence.length(); i++)
	{
		if (FOR_C == dna_sequence[i])
		{ //compare the DNAstrand to "G"
			this->_complementary_DNA_strand += FOR_G;
		}
		else if (FOR_G == dna_sequence[i])
		{ //compare the DNAstrand to "C"
			this->_complementary_DNA_strand += FOR_C;
		}
		else if (FOR_A == dna_sequence[i])
		{ //compare the DNAstrand to "T"
			this->_complementary_DNA_strand += FOR_T;
		}
		else if (FOR_T == dna_sequence[i])
		{ //compare the DNAstrand to "C"
			this->_complementary_DNA_strand += FOR_A;
		}
		else
		{ //if he letter are'nt A/G/T/C
			std::cerr << "An error has occurred :(";
			_exit(1);
		}
	}
}
/*
Take a specific part of the DNA and change T to U
if the parameter that check if it's a complementary is True
Input: Gene to use Take a specific part of his DNA
Output: The string that contains the specific DNA part with U instead of T
if the parameter that check if it's a complementary is True
*/
std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	int i = 0;
	std::string str = "";

	for (i = gene.get_start(); i < gene.get_end(); i++)
	{ //run from start - end
		str += this->_DNA_strand[i]; //get the string between the start - end
	}

	//change every 'T' in the string to 'U'
	str.replace(str.begin(), str.end(), FOR_A, FOR_RNA);

	return str;
}
/*
Reverse the DNA strand
Input: None
Output: Reversed string
*/
std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string reversedStrand = this->_DNA_strand; //reverse strand equals to the DNA strand

	std::reverse(reversedStrand.begin(), reversedStrand.end()); //reverse the DNA strand

	return reversedStrand;
}
/*
Get the numbers of times a codon appears in a DNA strand
Input: Codon to check the number of times it appears
Output: Number of times the codon appears
*/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	int i = 0;
	int j = 0;
	int num = 0;
	int flag = 0;

	for (i = 0; i < this->_DNA_strand.length(); i++)
	{ //go over the DNA strand
		if (codon[0] == this->_DNA_strand[i])
		{ //check if the first letter of the codon equals to a specific character
			for (j = 0; j < codon.length() && !flag; j++)
			{
				if (codon[j] != this->_DNA_strand[i])
				{ //check if the codon index is not equal to the DNA strand index
					flag = 1;
				}
				else if (j + 1 == codon.length())
				{ //check if it's the last itteration and the flag is still 0
					num++; //add 1 to the number of times the codon appears
				}
			}
		}
	}
	return num;
}
/*
Set the first strand name
Input: first strand
Output: New name as a string
*/
void Nucleus::setFirstStrand(const std::string firstS)
{
	this->_DNA_strand = firstS;
}
/*
Set the complementary strand name
Input: second strand
Output: New name as a string
*/
void Nucleus::setComplementaryStrand(const std::string secondS)
{
	this->_complementary_DNA_strand = secondS;
}
/*
Get the first strand name
Input: None
Output: New name as a string
*/
std::string Nucleus::getDNAStrand() const
{
	return this->_DNA_strand;
}
/*
Get the complementary strand name
Input: None
Output: New name as a string
*/
std::string Nucleus::getComplementaryStrand() const
{
	return this->_complementary_DNA_strand;
}
/*
Initlize the Gene
Input: start of the gene, end of the gene, 
true if the gene is on the complementary dna strand or false otherwise
Output: None
*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}
/*
Get the start of the Gene
Input: None
Output: The start of the Gene
*/
unsigned int Gene::get_start() const
{
	return this->_start;
}
/*
Get the end of the Gene
Input: None
Output: The end of the Gene
*/
unsigned int Gene::get_end() const
{
	return this->_end;
}
/*
Get if the gene is on the comlementary dna strand
Input: None
Output: if The gene is on the comlementary dna strand
*/
bool Gene::is_on_complementary_dna_strand()
{
	return this->_on_complementary_dna_strand;
}
/*
Set the start of the gene
Input: start of the gene
Output: None
*/
void Gene::setStart(const unsigned int start)
{
	this->_start = start;
}
/*
Set the end of the gene
Input: end of the gene
Output: None
*/
void Gene::setEnd(const unsigned int end)
{
	this->_end = end;
}
/*
Set the boolean value of complementary dna strand
Input: boolean value of complementary dna strand
Output: None
*/
void Gene::set_is_on_complementary_dna_strand(const bool on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}
