#include "Cell.h"

/*
Initlize the cell
Input: DNA sequence, Gene for glucose
*/
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_glocus_receptor_gene = _glocus_receptor_gene;
	this->_nucleus.init(dna_sequence);
	this->_mitochondrion.init();
}

bool Cell::get_ATP()
{
	bool _created_atp = true;
	std::string RNA = "";
	Protein* _protein;

	RNA = _nucleus.get_RNA_transcript(this->_glocus_receptor_gene); //1
	_protein = _ribosome.create_protein(RNA); //2

	if (NULL == _protein)
	{ //if the protein couldn't be created
		std::cout << ":(" << std::endl;
		_exit(1);
	}
	_mitochondrion.insert_glucose_receptor(*_protein); //3
	_mitochondrion.set_glucose(MIN_AMOUNT); //4
	_created_atp = _mitochondrion.produceATP(MIN_AMOUNT); //5

	if (!_created_atp)
	{ //if produce ATP did'nt worked
		return false;
	}

	this->_atp_units = ATP_UNITS; //6

	return true;
}
