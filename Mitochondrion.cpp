#include "Mitochondrion.h"

/*
Initlize the mitochondrion
Input: None
Output: None
*/
void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}
/*
Check if the protein is in an exact order and if so, _has_glocuse_receptor = true
Input: Protein to check
Output: None
*/
void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	AminoAcidNode* head;
	AminoAcidNode* curr;
	head = protein.get_first();
	curr = head;
	if (ALANINE == curr->get_data() && curr)
	{
		curr = curr->get_next(); //pass over 1 node
		if (LEUCINE == curr->get_data() && curr)
		{
			curr = curr->get_next(); //pass over 1 node
			if (GLYCINE == curr->get_data() && curr)
			{
				curr = curr->get_next(); //pass over 1 node
				if (HISTIDINE == curr->get_data() && curr)
				{
					curr = curr->get_next(); //pass over 1 node
					if (LEUCINE == curr->get_data() && curr)
					{
						curr = curr->get_next(); //pass over 1 node
						if (PHENYLALANINE == curr->get_data() && curr)
						{
							curr = curr->get_next(); //pass over 1 node
							if (AMINO_CHAIN_END == curr->get_data())
							{
								this->_has_glocuse_receptor = true;
							}
						}
					}
				}
			}
		}
	}
}
/*
Change the amount of glucose
Input: Amount of glucose
Output: None
*/
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}
/*
Check if the Mitochondrion can produce ATP
Input: The amount of glocuse
Output: if the Mitochondrion can produce ATP
*/
bool Mitochondrion::produceATP(const int glocuse_unit) const
{
	bool check = false;
	if (_has_glocuse_receptor && MIN_AMOUNT >= glocuse_unit)
	{ //check if the _has_glocuse_receptor = true and the amount of glocuse is bigger or equal to 50
		check = true;
	}
	return check;
}

